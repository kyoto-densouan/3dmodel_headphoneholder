# README #

スチールラックで使用するヘッドフォンホルダーのstlファイルです。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_headphoneholder/raw/f7c99d8586cb16e01f179bd807fcbf5cead9ad06/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_headphoneholder/raw/f7c99d8586cb16e01f179bd807fcbf5cead9ad06/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_headphoneholder/raw/f7c99d8586cb16e01f179bd807fcbf5cead9ad06/ExampleImage.jpg)
